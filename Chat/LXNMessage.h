//
//  LXNMessage.h
//  Chat
//
//  Created by Leszek Kaczor on 09/07/14.
//  Copyright (c) 2014 Untitled Kingdom. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "LXNMessageProtocol.h"

@interface LXNMessage : NSObject <LXNMessageProtocol>

@property (nonatomic, strong) NSString * message;
@property (nonatomic, strong) NSString * authorId;
@property (nonatomic, strong) NSString * authorName;
@property (nonatomic, strong) NSString * authorImageURL;
@property (nonatomic, strong) NSDate   * date;

+ (instancetype)getDefault;
+ (instancetype)getDefaultWithChangedAuthor;
+ (instancetype)getDefaultWithChangedDate;
+ (instancetype)getDefaultWithChangedDateAndAuthor;

@end
