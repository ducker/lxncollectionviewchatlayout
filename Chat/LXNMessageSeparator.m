//
//  LXNMessageSeparator.m
//  Chat
//
//  Created by Leszek Kaczor on 26/08/14.
//  Copyright (c) 2014 Untitled Kingdom. All rights reserved.
//

#import "LXNMessageSeparator.h"

@implementation LXNMessageSeparator

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self setup];
    }
    return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self) {
        [self setup];
    }
    return self;
}

- (void)setup
{
    self.backgroundColor = [UIColor orangeColor];
}

@end
