//
//  LXNViewController.m
//  Chat
//
//  Created by Leszek Kaczor on 07/07/14.
//  Copyright (c) 2014 Untitled Kingdom. All rights reserved.
//

#import "LXNViewController.h"
#import "LXNChatDataSource.h"
#import "LXNCollectionViewChatLayout.h"
#import "LXNMessage.h"
#import "LXNContactHeader.h"
#import "LXNDayHeader.h"
#import "LXNMessageSeparator.h"

@interface LXNViewController () <LXNChatDataSourceDelegate>

@property (strong, nonatomic) LXNChatDataSource * layoutDataSource;

@end

@implementation LXNViewController

#pragma mark - Initialization
- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    self.layoutDataSource = [[LXNChatDataSource alloc] init];
    self.layoutDataSource.delegate = self;
    
    [self.collectionView registerClass:[LXNContactHeader class] forSupplementaryViewOfKind:contactHeaderViewKind withReuseIdentifier:@"reuseContactHeader"];
    [self.collectionView registerClass:[LXNDayHeader class] forSupplementaryViewOfKind:dayHeaderViewKind withReuseIdentifier:@"reuseDayHeader"];
    [self.collectionView.collectionViewLayout registerClass:[LXNMessageSeparator class] forDecorationViewOfKind:messageSeparatorViewKind];
    
    self.collectionView.dataSource = self.layoutDataSource;
    
    [self.layoutDataSource setConfigureCellBlock:^UICollectionViewCell *(UICollectionView *collectionView, NSIndexPath *indexPath, id<LXNMessageProtocol> messageObject) {
        UICollectionViewCell * cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"collectionViewCell" forIndexPath:indexPath];
        cell.backgroundColor = [UIColor greenColor];
        return cell;
    }];
    
    [self.layoutDataSource setConfigureSupplementaryViewBlock:^UICollectionReusableView *(UICollectionView *collectionView, NSIndexPath *indexPath, NSString *kind, NSArray *sectionDataArray) {
        UICollectionReusableView * reusableView = nil;
        if ([kind isEqualToString:contactHeaderViewKind])
        {
            reusableView = [collectionView dequeueReusableSupplementaryViewOfKind:kind withReuseIdentifier:@"reuseContactHeader" forIndexPath:indexPath];
            reusableView.backgroundColor = [UIColor redColor];
        }
        if ([kind isEqualToString:dayHeaderViewKind])
        {
            reusableView = [collectionView dequeueReusableSupplementaryViewOfKind:kind withReuseIdentifier:@"reuseDayHeader" forIndexPath:indexPath];
            reusableView.backgroundColor = [UIColor blueColor];
        }
        return reusableView;
    }];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    LXNCollectionViewChatLayout * layout = (LXNCollectionViewChatLayout *)self.collectionView.collectionViewLayout;
    layout.dataSource = self.layoutDataSource;
    
    LXNMessage * obj = [LXNMessage getDefault];
    LXNMessage * obj2 = [LXNMessage getDefault];
    LXNMessage * obj3 = [LXNMessage getDefaultWithChangedAuthor];
    LXNMessage * obj4 = [LXNMessage getDefaultWithChangedAuthor];
    
    LXNMessage * obj5 = [LXNMessage getDefaultWithChangedDate];
    LXNMessage * obj6 = [LXNMessage getDefaultWithChangedDate];
    LXNMessage * obj7 = [LXNMessage getDefaultWithChangedDateAndAuthor];
    LXNMessage * obj8 = [LXNMessage getDefaultWithChangedDateAndAuthor];
    
    NSArray * messages =  @[obj, obj2, obj3, obj4, obj5, obj6, obj7, obj8];
    messages = [messages arrayByAddingObjectsFromArray:messages];
    
    self.layoutDataSource.dataArray = messages;
    [self.collectionView reloadData];
}

#pragma mark - Private
- (NSDate *)getDateWithoutHoursFromDate:(NSDate *)date
{
    static NSDateFormatter *dateFormatter = nil;
    
    if (!dateFormatter)
    {
        dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateStyle:NSDateFormatterFullStyle];
        [dateFormatter setTimeStyle:NSDateFormatterNoStyle];
    }
    NSString *dateString = [dateFormatter stringFromDate:date];
    return [dateFormatter dateFromString:dateString];
}

#pragma mark - LXNChatDataSourceDelegate
- (CGSize)chatDataSource:(LXNChatDataSource *)chatDataSource cellSizeForMessage:(id<LXNMessageProtocol>)message
{
    return CGSizeMake(200, rand()%100 + 100);
}

- (CGSize)chatDataSource:(LXNChatDataSource *)chatDataSource dayHeaderSizeForSectionDataArray:(NSArray *)sectionArray previousSectionDataArray:(NSArray *)prevSectionArray;

{
    if (!prevSectionArray) return CGSizeMake(self.view.bounds.size.width, 30.0f);
    LXNMessage * message = sectionArray.lastObject;
    LXNMessage * prevMessage = prevSectionArray.lastObject;
    
    NSDate * messageDate = [self getDateWithoutHoursFromDate:message.date];
    NSDate * prevMessageDate = [self getDateWithoutHoursFromDate:prevMessage.date];
    
    if (![messageDate isEqualToDate:prevMessageDate])
        return CGSizeMake(self.view.bounds.size.width, 30.0f);
    return CGSizeZero;
}

- (CGSize)chatDataSource:(LXNChatDataSource *)chatDataSource contactHeaderSizeForSectionDataArray:(NSArray *)array
{
    return CGSizeMake(80, 150);
}

- (BOOL)chatDataSource:(LXNChatDataSource *)chatDataSource contactHeaderOnLeftForSectionDataArray:(NSArray *)array
{
    LXNMessage * message = array.firstObject;
    if ([message.authorId isEqualToString:@"ABC"])
        return YES;
    return NO;
}

- (CGFloat)chatDataSource:(LXNChatDataSource *)chatDataSource cellOffsetFromContactHeaderForMessage:(id<LXNMessageProtocol>)message
{
    return 10.0f;
}

- (CGFloat)cellSeparatorHeightForChatDataSource:(LXNChatDataSource *)chatDataSource
{
    return 1.0f;
}

- (UIEdgeInsets)cellSeparatorInsetsChatDataSource:(LXNChatDataSource *)chatDataSource
{
    return UIEdgeInsetsMake(0, 10, 0, 10);
}

- (BOOL)cellSeparatorIsPartOfCellForChatDataSource:(LXNChatDataSource *)chatDataSource
{
    return NO;
}

@end
