//
//  LXNViewController.h
//  Chat
//
//  Created by Leszek Kaczor on 07/07/14.
//  Copyright (c) 2014 Untitled Kingdom. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LXNViewController : UIViewController

@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;

@end
