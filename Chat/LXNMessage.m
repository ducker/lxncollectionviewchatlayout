//
//  LXNMessage.m
//  Chat
//
//  Created by Leszek Kaczor on 09/07/14.
//  Copyright (c) 2014 Untitled Kingdom. All rights reserved.
//

#import "LXNMessage.h"

@implementation LXNMessage

+ (instancetype)getDefault
{
    LXNMessage * obj = [[LXNMessage alloc] init];
    obj.message = @"test message";
    obj.authorId = @"ABC";
    obj.authorName = @"ABC CDE";
    obj.authorImageURL = nil;
    obj.date = [NSDate date];
    return obj;
}

+ (instancetype)getDefaultWithChangedAuthor
{
    LXNMessage * obj = [[LXNMessage alloc] init];
    obj.message = @"test message";
    obj.authorId = @"CDE";
    obj.authorName = @"CDE FGH";
    obj.authorImageURL = nil;
    obj.date = [NSDate date];
    return obj;
}

+ (instancetype)getDefaultWithChangedDate
{
    LXNMessage * obj = [[LXNMessage alloc] init];
    obj.message = @"test message";
    obj.authorId = @"ABC";
    obj.authorName = @"ABC CDE";
    obj.authorImageURL = nil;
    obj.date = [[NSDate date] dateByAddingTimeInterval:60*60*24];
    return obj;
}

+ (instancetype)getDefaultWithChangedDateAndAuthor
{
    LXNMessage * obj = [[LXNMessage alloc] init];
    obj.message = @"test message";
    obj.authorId = @"CDE";
    obj.authorName = @"CDE FGH";
    obj.authorImageURL = nil;
    obj.date = [[NSDate date] dateByAddingTimeInterval:60*60*24];
    return obj;
}

@end
