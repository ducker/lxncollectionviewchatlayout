//
//  LXNDayHeader.m
//  Chat
//
//  Created by Leszek Kaczor on 09/07/14.
//  Copyright (c) 2014 Untitled Kingdom. All rights reserved.
//

#import "LXNDayHeader.h"

@implementation LXNDayHeader

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self)
    {
    }
    return self;
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self)
    {
    }
    return self;
}

@end
