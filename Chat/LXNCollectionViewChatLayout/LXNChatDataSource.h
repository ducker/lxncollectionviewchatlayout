//
//  LXNChatDataSource.h
//  Chat
//
//  Created by Leszek Kaczor on 07/07/14.
//  Copyright (c) 2014 Untitled Kingdom. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "LXNCollectionViewChatLayout.h"
#import "LXNMessageProtocol.h"

typedef UICollectionViewCell*(^ConfigureCellBlock)(UICollectionView * collectionView, NSIndexPath * indexPath, id<LXNMessageProtocol> messageObject);
typedef UICollectionReusableView*(^ConfigureSupplementaryViewBlock)(UICollectionView * collectionView, NSIndexPath * indexPath, NSString * kind, NSArray * sectionDataArray);

@class LXNChatDataSource;
@protocol LXNChatDataSourceDelegate <NSObject>

@required
- (CGSize)chatDataSource:(LXNChatDataSource *)chatDataSource cellSizeForMessage:(id<LXNMessageProtocol>)message;
- (CGSize)chatDataSource:(LXNChatDataSource *)chatDataSource dayHeaderSizeForSectionDataArray:(NSArray *)sectionArray previousSectionDataArray:(NSArray *)prevSectionArray;
- (CGSize)chatDataSource:(LXNChatDataSource *)chatDataSource contactHeaderSizeForSectionDataArray:(NSArray *)array;
- (BOOL)chatDataSource:(LXNChatDataSource *)chatDataSource contactHeaderOnLeftForSectionDataArray:(NSArray *)array;

@optional
- (CGFloat)chatDataSource:(LXNChatDataSource *)chatDataSource cellOffsetFromContactHeaderForMessage:(id<LXNMessageProtocol>)message;    // default: 0.0f
- (BOOL)cellSeparatorIsPartOfCellForChatDataSource:(LXNChatDataSource *)chatDataSource;         // default: NO
- (UIEdgeInsets)cellSeparatorInsetsChatDataSource:(LXNChatDataSource *)chatDataSource;          // default: UIEdgeInsetsZero
- (CGFloat)cellSeparatorHeightForChatDataSource:(LXNChatDataSource *)chatDataSource;            // default: 0.0f

@end

@interface LXNChatDataSource : NSObject <UICollectionViewDataSource, LXNCollectionViewChatLayoutDataSource>

@property (strong, nonatomic) NSArray * dataArray;
@property (weak, nonatomic  ) IBOutlet id<LXNChatDataSourceDelegate> delegate;

- (void)setConfigureCellBlock:(ConfigureCellBlock)block;
- (void)setConfigureSupplementaryViewBlock:(ConfigureSupplementaryViewBlock)block;

@end
