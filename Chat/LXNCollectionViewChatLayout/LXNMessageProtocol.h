//
//  LXNMessageProtocol.h
//  Chat
//
//  Created by Leszek Kaczor on 07/07/14.
//  Copyright (c) 2014 Untitled Kingdom. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol LXNMessageProtocol <NSObject>

@property (nonatomic, strong) id       authorId;    // To group messages by author
@property (nonatomic, strong) NSDate   * date;      // To group messages by date

@end
