//
//  LXNChatDataSource.m
//  Chat
//
//  Created by Leszek Kaczor on 07/07/14.
//  Copyright (c) 2014 Untitled Kingdom. All rights reserved.
//

#import "LXNChatDataSource.h"
#import "LXNMessageProtocol.h"

@interface LXNChatDataSource()
/*
 localDataArray structure is:
    localDataArray
        |_ NSArrays groupped by day
            |_ NSArrays with messages groupped by author
 */
@property (strong, nonatomic) NSArray * localDataArray;

@property (copy, nonatomic) ConfigureCellBlock              configureCellBlock;
@property (copy, nonatomic) ConfigureSupplementaryViewBlock configureSupplementaryViewBlock;

@end

@implementation LXNChatDataSource

#pragma mark - Public
- (void)setConfigureCellBlock:(ConfigureCellBlock)block
{
    _configureCellBlock = block;
}

- (void)setConfigureSupplementaryViewBlock:(ConfigureSupplementaryViewBlock)block
{
    _configureSupplementaryViewBlock = block;
}

#pragma mark - Custom accessors
- (void)setDataArray:(NSArray *)dataArray
{
    _dataArray = dataArray;
    [self configureLocalDataArray:dataArray];
}

#pragma mark - Private

- (void)configureLocalDataArray:(NSArray *)array
{
    NSMutableArray * mutableLocalArray = [NSMutableArray array];
    NSArray * arrayGrouppedByDates = [self groupMessagesByDateFromArray:array];
    for (NSArray * arr in arrayGrouppedByDates)
        [mutableLocalArray addObject:[self groupMessagesByAuthorFromArray:arr]];
    self.localDataArray = [mutableLocalArray copy];
}

- (NSArray *)groupMessagesByDateFromArray:(NSArray *)array
{
    NSMutableArray * mutableLocalArray = [NSMutableArray array];
    NSMutableArray * currentDayArray = [NSMutableArray array];
    NSDate * prevDay = nil;
    for (id<LXNMessageProtocol> message in array)
    {
        NSDate * dateWithoutTime = [self getDateWithoutHoursFromDate:message.date];
        if (!prevDay) prevDay = dateWithoutTime;
        if (![prevDay isEqualToDate:dateWithoutTime])
        {
            [mutableLocalArray addObject:currentDayArray];
            currentDayArray = [NSMutableArray array];
            [currentDayArray addObject:message];
            prevDay = dateWithoutTime;
        } else {
            [currentDayArray addObject:message];
        }
    }
    if (currentDayArray.count) [mutableLocalArray addObject:currentDayArray];
    return [mutableLocalArray copy];
}

- (NSDate *)getDateWithoutHoursFromDate:(NSDate *)date
{
    static NSDateFormatter *dateFormatter = nil;
    
    if (!dateFormatter)
    {
        dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateStyle:NSDateFormatterFullStyle];
        [dateFormatter setTimeStyle:NSDateFormatterNoStyle];
    }
    NSString *dateString = [dateFormatter stringFromDate:date];
    return [dateFormatter dateFromString:dateString];
}

- (NSArray *)groupMessagesByAuthorFromArray:(NSArray *)array
{
    NSMutableArray * newMutableArray = [NSMutableArray array];
    NSMutableArray * currentConversation = [NSMutableArray array];
    id prevId = nil;
    for (id<LXNMessageProtocol> message in array)
    {
        if (!prevId) prevId = message.authorId;
        if (![prevId isEqual:message.authorId])
        {
            [newMutableArray addObject:currentConversation];
            currentConversation = [NSMutableArray array];
            [currentConversation addObject:message];
            prevId = message.authorId;
        } else {
            [currentConversation addObject:message];
        }
    }
    if (currentConversation.count) [newMutableArray addObject:currentConversation];
    return [newMutableArray copy];
}

- (id<LXNMessageProtocol>)objectForIndexPath:(NSIndexPath *)indexPath
{
    id<LXNMessageProtocol> messageObject = nil;
    
    NSArray * sectionMessages = [self sectionDataArrayForIndex:indexPath.section];
    if (indexPath.item < sectionMessages.count)
        messageObject = sectionMessages[indexPath.item];

    return messageObject;
}

- (NSArray *)sectionDataArrayForIndex:(NSInteger)section
{
    for (NSArray * dayArray in self.localDataArray)
        for (NSArray * messageArray in dayArray)
            if (section-- == 0)
                return messageArray;
    return nil;
}

- (NSInteger)numberOfSections
{
    NSInteger count = 0;
    for (NSArray * array in self.localDataArray)
        count += array.count;
    return count;
}

#pragma mark - LXNCollectionViewChatLayoutDataSource

- (CGSize)chatLayout:(LXNCollectionViewChatLayout *)layout cellSizeForIndexPath:(NSIndexPath *)indexPath
{
    NSAssert(self.delegate, @"Chat layout delegate is nil");
    return [self.delegate chatDataSource:self cellSizeForMessage:[self objectForIndexPath:indexPath]];
}

- (CGSize)chatLayout:(LXNCollectionViewChatLayout *)layout dayHeaderSizeForSection:(NSInteger)section
{
    NSAssert(self.delegate, @"Chat layout delegate is nil");
    NSArray * sectionDataArray = [self sectionDataArrayForIndex:section];
    NSArray * prevSectionDataArray = section > 0 ? [self sectionDataArrayForIndex:section-1] : nil;
    return [self.delegate chatDataSource:self dayHeaderSizeForSectionDataArray:sectionDataArray previousSectionDataArray:prevSectionDataArray];
}

- (CGSize)chatLayout:(LXNCollectionViewChatLayout *)layout contactHeaderViewSizeForSection:(NSInteger)section
{
    NSAssert(self.delegate, @"Chat layout delegate is nil");
    return [self.delegate chatDataSource:self contactHeaderSizeForSectionDataArray:[self sectionDataArrayForIndex:section]];
}

- (BOOL)chatLayout:(LXNCollectionViewChatLayout *)layout contactHeaderOnLeftForSection:(NSInteger)section
{
    NSAssert(self.delegate, @"Chat layout delegate is nil");
    return [self.delegate chatDataSource:self contactHeaderOnLeftForSectionDataArray:[self sectionDataArrayForIndex:section]];
}

- (CGFloat)chatLayout:(LXNCollectionViewChatLayout *)layout cellOffsetFromContactHeaderForIndexPath:(NSIndexPath *)indexPath
{
    NSAssert(self.delegate, @"Chat layout delegate is nil");
    CGFloat result = 0.0f;
    if ([self.delegate respondsToSelector:@selector(chatDataSource:cellOffsetFromContactHeaderForMessage:)])
        result = [self.delegate chatDataSource:self cellOffsetFromContactHeaderForMessage:[self objectForIndexPath:indexPath]];
    return result;
}

- (BOOL)cellSeparatorIsPartOfCellForChatLayout:(LXNCollectionViewChatLayout *)layout
{
    NSAssert(self.delegate, @"Chat layout delegate is nil");
    BOOL result = NO;
    if ([self.delegate respondsToSelector:@selector(cellSeparatorIsPartOfCellForChatDataSource:)])
        result = [self.delegate cellSeparatorIsPartOfCellForChatDataSource:self];
    return result;
}

- (UIEdgeInsets)cellSeparatorInsetsForChatLayout:(LXNCollectionViewChatLayout *)layout
{
    NSAssert(self.delegate, @"Chat layout delegate is nil");
    UIEdgeInsets result = UIEdgeInsetsZero;
    if ([self.delegate respondsToSelector:@selector(cellSeparatorInsetsChatDataSource:)])
        result = [self.delegate cellSeparatorInsetsChatDataSource:self];
    return result;
}

- (CGFloat)cellSeparatorHeightForChatLayout:(LXNCollectionViewChatLayout *)layout
{
    NSAssert(self.delegate, @"Chat layout delegate is nil");
    CGFloat result = 0.0f;
    if ([self.delegate respondsToSelector:@selector(cellSeparatorHeightForChatDataSource:)])
         result = [self.delegate cellSeparatorHeightForChatDataSource:self];
     return result;
}


#pragma mark - UICollectionViewDataSource

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return [self numberOfSections];
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return [[self sectionDataArrayForIndex:section] count];
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    if (self.configureCellBlock)
        return self.configureCellBlock(collectionView, indexPath, [self objectForIndexPath:indexPath]);
    return nil;
}

- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath
{
    NSAssert([kind isEqualToString:dayHeaderViewKind] || [kind isEqualToString:contactHeaderViewKind], @"Incorrect supplementary view kind");
    if (self.configureSupplementaryViewBlock)
        return self.configureSupplementaryViewBlock(collectionView, indexPath, kind, [self sectionDataArrayForIndex:indexPath.section]);
    return nil;
}

@end
