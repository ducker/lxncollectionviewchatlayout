//
//  LXNCollectionViewChatLayout.h
//  Chat
//
//  Created by Leszek Kaczor on 07/07/14.
//  Copyright (c) 2014 Untitled Kingdom. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LXNMessageProtocol.h"

extern NSString * const dayHeaderViewKind;
extern NSString * const contactHeaderViewKind;
extern NSString * const messageSeparatorViewKind;

@class LXNCollectionViewChatLayout;
@protocol LXNCollectionViewChatLayoutDataSource <NSObject>

- (CGSize)chatLayout:(LXNCollectionViewChatLayout *)layout cellSizeForIndexPath:(NSIndexPath *)indexPath;
- (CGSize)chatLayout:(LXNCollectionViewChatLayout *)layout dayHeaderSizeForSection:(NSInteger)section;
- (CGFloat)chatLayout:(LXNCollectionViewChatLayout *)layout cellOffsetFromContactHeaderForIndexPath:(NSIndexPath *)indexPath;
- (CGSize)chatLayout:(LXNCollectionViewChatLayout *)layout contactHeaderViewSizeForSection:(NSInteger)section;
- (CGFloat)cellSeparatorHeightForChatLayout:(LXNCollectionViewChatLayout *)layout;
- (UIEdgeInsets)cellSeparatorInsetsForChatLayout:(LXNCollectionViewChatLayout *)layout;
- (BOOL)cellSeparatorIsPartOfCellForChatLayout:(LXNCollectionViewChatLayout*)layout;
- (BOOL)chatLayout:(LXNCollectionViewChatLayout*)layout contactHeaderOnLeftForSection:(NSInteger)section;

@end

@interface LXNCollectionViewChatLayout : UICollectionViewFlowLayout

@property (weak, nonatomic) IBOutlet id<LXNCollectionViewChatLayoutDataSource>dataSource;

@end
