//
//  LXNCollectionViewChatLayout.m
//  Chat
//
//  Created by Leszek Kaczor on 07/07/14.
//  Copyright (c) 2014 Untitled Kingdom. All rights reserved.
//

#import "LXNCollectionViewChatLayout.h"

NSString * const dayHeaderViewKind          = @"DayHeaderView";
NSString * const contactHeaderViewKind      = @"ContactHeaderView";
NSString * const messageCellView            = @"MessageCellView";
NSString * const messageSeparatorViewKind   = @"MessageSeparatorView";

@interface LXNCollectionViewChatLayout()

@property (strong, nonatomic) NSDictionary * layoutInfo;
@property (assign, nonatomic) CGFloat      totalHeight;

@end

@implementation LXNCollectionViewChatLayout

#pragma mark - Private
#pragma mark - Attributes for rect

- (NSArray *)attributesOfItemsInRect:(CGRect)rect
{
    NSMutableArray * returnArray = [NSMutableArray array];
    NSDictionary *attributesDict = [self.layoutInfo objectForKey:messageCellView];
    for(NSIndexPath *key in attributesDict){
        UICollectionViewLayoutAttributes *attributes =
        [attributesDict objectForKey:key];
        if(CGRectIntersectsRect(rect, attributes.frame)){
            [returnArray addObject:attributes];
        }
    }
    return [returnArray copy];
}

- (NSArray *)attributesOfContactHeaderViewsInRect:(CGRect)rect
{
    NSMutableArray * returnArray = [NSMutableArray array];
    NSDictionary *attributesDict = [self.layoutInfo objectForKey:contactHeaderViewKind];
    for(NSIndexPath *key in attributesDict){
        UICollectionViewLayoutAttributes *attributes =
        [attributesDict objectForKey:key];
        if(CGRectIntersectsRect(rect, attributes.frame)){
            [returnArray addObject:attributes];
        }
    }
    return [returnArray copy];
}

- (NSArray *)attributesOfDayHeaderViewsInRect:(CGRect)rect
{
    NSMutableArray * returnArray = [NSMutableArray array];
    NSDictionary *attributesDict = [self.layoutInfo objectForKey:dayHeaderViewKind];
    for(NSIndexPath *key in attributesDict){
        UICollectionViewLayoutAttributes *attributes =
        [attributesDict objectForKey:key];
        if(CGRectIntersectsRect(rect, attributes.frame)){
            [returnArray addObject:attributes];
        }
    }
    return [returnArray copy];
}

- (NSArray *)attributesOfSeparatorViewsInRect:(CGRect)rect
{
    NSMutableArray * returnArray = [NSMutableArray array];
    NSDictionary *attributesDict = [self.layoutInfo objectForKey:messageSeparatorViewKind];
    for(NSIndexPath *key in attributesDict){
        UICollectionViewLayoutAttributes *attributes =
        [attributesDict objectForKey:key];
        if(CGRectIntersectsRect(rect, attributes.frame)){
            [returnArray addObject:attributes];
        }
    }
    return [returnArray copy];
}

#pragma mark - Updating attributes
- (void)updateAttributesForContactHeader:(UICollectionViewLayoutAttributes *)attributes
                       withSectionOffset:(CGFloat)offset
                              forSection:(NSUInteger)section
{
    CGRect frame = attributes.frame;
    frame.size = [self.dataSource chatLayout:self contactHeaderViewSizeForSection:section];
    frame.origin.y = offset;
    if ([self.dataSource chatLayout:self contactHeaderOnLeftForSection:section])
        frame.origin.x = 0.0f;
    else
        frame.origin.x = [self collectionViewContentSize].width - frame.size.width;
    attributes.frame = frame;
}

- (void)updateAttributesForCell:(UICollectionViewLayoutAttributes *)attributes
              withSectionOffset:(CGFloat)offset
                 withCellOffset:(CGFloat)cellOffset
             contactHeaderwidth:(CGFloat)contactHeaderWidth
                   forIndexPath:(NSIndexPath *)indexPath
{
    CGRect frame = attributes.frame;
    frame.size = [self.dataSource chatLayout:self cellSizeForIndexPath:indexPath];
    if ([self.dataSource chatLayout:self contactHeaderOnLeftForSection:indexPath.section])
        frame.origin.x = contactHeaderWidth + [self.dataSource chatLayout:self cellOffsetFromContactHeaderForIndexPath:indexPath];
    else
        frame.origin.x = [self collectionViewContentSize].width - frame.size.width - [self.dataSource chatLayout:self cellOffsetFromContactHeaderForIndexPath:indexPath] - contactHeaderWidth;
    frame.origin.y = offset + cellOffset;
    attributes.frame = frame;


}

- (void)updateAttributesForSeparator:(UICollectionViewLayoutAttributes *)separatorAttributes
                  withCellAttributes:(UICollectionViewLayoutAttributes *)cellAttributes
                        isPartOfCell:(BOOL)isPartOfCell
                   withSectionOffset:(CGFloat)offset
                      withCellOffset:(CGFloat)cellOffset
                        forIndexPath:(NSIndexPath *)indexPath
{
    CGFloat separatorHeight = [self.dataSource cellSeparatorHeightForChatLayout:self];
    UIEdgeInsets separatorInsets = [self.dataSource cellSeparatorInsetsForChatLayout:self];
    CGPoint cellOrigin = cellAttributes.frame.origin;
    CGSize cellSize = cellAttributes.frame.size;
    if (isPartOfCell)
        separatorAttributes.frame = CGRectMake(cellOrigin.x + separatorInsets.left,
                                               offset + cellOffset - separatorHeight,
                                               cellSize.width - (separatorInsets.left + separatorInsets.right),
                                               separatorHeight);
    else
        separatorAttributes.frame = CGRectMake(cellOrigin.x + separatorInsets.left,
                                               offset + cellOffset,
                                               cellSize.width - (separatorInsets.left + separatorInsets.right),
                                               separatorHeight);
    separatorAttributes.zIndex = 1;
}


#pragma mark - Override UICollectionViewLayour
- (void)prepareLayout
{
    NSMutableDictionary *newLayoutInfo = [NSMutableDictionary dictionary];
    NSMutableDictionary *cellLayoutInfo = [NSMutableDictionary dictionary];
    NSMutableDictionary *dayHeaderLayoutInfo = [NSMutableDictionary dictionary];
    NSMutableDictionary *contactHeaderLayoutInfo = [NSMutableDictionary dictionary];
    NSMutableDictionary *cellSeparatorsLayoutInfo = [NSMutableDictionary dictionary];
    
    NSInteger sectionCount = [self.collectionView.dataSource numberOfSectionsInCollectionView:self.collectionView];
    NSIndexPath *indexPath = [NSIndexPath indexPathForItem:0 inSection:0];
    
    CGPoint currentSectionOrigin = CGPointZero;
    CGSize currentContactHeaderSize = CGSizeZero;
    CGSize currentDayHeaderSize = CGSizeZero;
    
    for (NSInteger section = 0; section < sectionCount; section++)
    {
        //////////////////////////////////////////
        // Day header
        currentDayHeaderSize = [self.dataSource chatLayout:self dayHeaderSizeForSection:section];
        if (currentDayHeaderSize.height > 0)
        {
            UICollectionViewLayoutAttributes * dayAttributes = [self layoutAttributesForSupplementaryViewOfKind:dayHeaderViewKind atIndexPath:[NSIndexPath indexPathForItem:0 inSection:section]];
            CGRect frame = dayAttributes.frame;
            frame.origin = currentSectionOrigin;
            frame.size = currentDayHeaderSize;
            dayAttributes.frame = frame;
            [dayHeaderLayoutInfo setObject:dayAttributes forKey:indexPath];
        }
        currentSectionOrigin.y += currentDayHeaderSize.height;
        
        //////////////////////////////////////////
        // Contact header
        UICollectionViewLayoutAttributes * contactAttributes = [self layoutAttributesForSupplementaryViewOfKind:contactHeaderViewKind atIndexPath:[NSIndexPath indexPathForItem:0 inSection:section]];
        [self updateAttributesForContactHeader:contactAttributes withSectionOffset:currentSectionOrigin.y forSection:section];
        currentContactHeaderSize = contactAttributes.frame.size;
        [contactHeaderLayoutInfo setObject:contactAttributes forKey:indexPath];
        
        /////////////////////////////////////////
        // Cells
        NSInteger itemCount = [self.collectionView.dataSource collectionView:self.collectionView numberOfItemsInSection:section];
        CGFloat currentCellOffset = 0.0f;
        for (NSInteger item = 0; item < itemCount; item++)
        {
            indexPath = [NSIndexPath indexPathForItem:item inSection:section];
            UICollectionViewLayoutAttributes * attributes = [UICollectionViewLayoutAttributes layoutAttributesForCellWithIndexPath:indexPath];
            [self updateAttributesForCell:attributes
                        withSectionOffset:currentSectionOrigin.y
                           withCellOffset:currentCellOffset
                       contactHeaderwidth:currentContactHeaderSize.width
                             forIndexPath:indexPath];
            currentCellOffset += attributes.frame.size.height;
            [cellLayoutInfo setObject:attributes forKey:indexPath];
            /////////////////////////////////////////
            // Separators
            if (item < itemCount - 1 && [self.dataSource cellSeparatorHeightForChatLayout:self] > 0)
            {
                BOOL separatorIsPartOfCell = [self.dataSource cellSeparatorIsPartOfCellForChatLayout:self];
                CGFloat separatorHeight = [self.dataSource cellSeparatorHeightForChatLayout:self];
                UICollectionViewLayoutAttributes * separatorAttributes = [UICollectionViewLayoutAttributes layoutAttributesForDecorationViewOfKind:messageSeparatorViewKind withIndexPath:indexPath];
                [self updateAttributesForSeparator:separatorAttributes
                                withCellAttributes:attributes
                                      isPartOfCell:separatorIsPartOfCell
                                 withSectionOffset:currentSectionOrigin.y
                                    withCellOffset:currentCellOffset
                                      forIndexPath:indexPath];
                if (!separatorIsPartOfCell)
                    currentCellOffset += separatorHeight;
                [cellSeparatorsLayoutInfo setObject:separatorAttributes forKey:indexPath];
            }
        }
        currentSectionOrigin.y += MAX(currentContactHeaderSize.height, currentCellOffset);
    }
    
    self.totalHeight = currentSectionOrigin.y;
    
    newLayoutInfo[messageCellView]          = cellLayoutInfo;
    newLayoutInfo[dayHeaderViewKind]        = dayHeaderLayoutInfo;
    newLayoutInfo[contactHeaderViewKind]    = contactHeaderLayoutInfo;
    newLayoutInfo[messageSeparatorViewKind] = cellSeparatorsLayoutInfo;
    self.layoutInfo = newLayoutInfo;
}

- (CGSize)collectionViewContentSize
{
    CGFloat contentWidth = self.collectionView.bounds.size.width;
    CGFloat contentHeight = self.totalHeight;
    
    CGSize contentSize = CGSizeMake(contentWidth, contentHeight);
    return contentSize;
}

- (NSArray *)layoutAttributesForElementsInRect:(CGRect)rect
{
    NSMutableArray *layoutAttributes = [NSMutableArray array];
    
    [layoutAttributes addObjectsFromArray:[self attributesOfItemsInRect:rect]];
    [layoutAttributes addObjectsFromArray:[self attributesOfDayHeaderViewsInRect:rect]];
    [layoutAttributes addObjectsFromArray:[self attributesOfContactHeaderViewsInRect:rect]];
    [layoutAttributes addObjectsFromArray:[self attributesOfSeparatorViewsInRect:rect]];

    return layoutAttributes;
}

- (UICollectionViewLayoutAttributes *)layoutAttributesForDecorationViewOfKind:(NSString *)decorationViewKind atIndexPath:(NSIndexPath *)indexPath
{
    if ([decorationViewKind isEqualToString:messageSeparatorViewKind])
    {
        UICollectionViewLayoutAttributes * attributes = [UICollectionViewLayoutAttributes layoutAttributesForDecorationViewOfKind:decorationViewKind withIndexPath:indexPath];
        return attributes;
    }
    return [super layoutAttributesForDecorationViewOfKind:decorationViewKind atIndexPath:indexPath];
}

- (UICollectionViewLayoutAttributes *)layoutAttributesForSupplementaryViewOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath
{
    if ([kind isEqualToString:dayHeaderViewKind])
    {
        UICollectionViewLayoutAttributes * attributes = [UICollectionViewLayoutAttributes layoutAttributesForSupplementaryViewOfKind:kind withIndexPath:indexPath];
        return attributes;
    }
    if ([kind isEqualToString:contactHeaderViewKind])
    {
        UICollectionViewLayoutAttributes * attributes = [UICollectionViewLayoutAttributes layoutAttributesForSupplementaryViewOfKind:kind withIndexPath:indexPath];
        return attributes;
    }
    return [super layoutAttributesForSupplementaryViewOfKind:kind atIndexPath:indexPath];
}

- (BOOL)shouldInvalidateLayoutForBoundsChange:(CGRect)newBounds
{
    CGRect oldBounds = self.collectionView.bounds;
    if (CGRectGetWidth(newBounds) != CGRectGetWidth(oldBounds))
        return YES;
    return NO;
}

@end
