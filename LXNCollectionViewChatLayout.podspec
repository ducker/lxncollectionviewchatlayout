Pod::Spec.new do |s|
  s.name                  = 'LXNCollectionViewChatLayout'
  s.version               = '0.1.0'
  s.summary               = ''
  s.author                = { 'Leszek Kaczor' => 'leszekducker@gmail.com' }
  s.source                = { :git => 'https://bitbucket.org/ducker/lxncollectionviewchatlayout.git' }
  s.source_files          = 'Chat/LXNCollectionViewCHatLayout/*'
  s.requires_arc 	  = true
end
