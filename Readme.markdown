## LXNCollectionViewChatLayout 

This is the repository for LXNCollectionViewChatLayout.

## Layout structure
![Chat layout structure.png](https://bitbucket.org/repo/bAyXjX/images/3556689605-Chat%20layout%20structure.png)

## Installation

Add following line to your Podfile

```
pod 'LXNCollectionViewChatLayout', :git => "https://bitbucket.org/ducker/lxncollectionviewchatlayout.git"
```

If no cocoapods installed go to http://cocoapods.org for further informations.

## Usage

Change layout of your collection view to LXNCollectionViewChatLayout. After that you should initialize LXNChatDataSource and configure it.

```objective-c
self.layoutDataSource = [[LXNChatDataSource alloc] init];

LXNCollectionViewChatLayout * layout = (LXNCollectionViewChatLayout *)self.collectionView.collectionViewLayout;
layout.dataSource = self.layoutDataSource;

[self.layoutDataSource setConfigureCellBlock:^UICollectionViewCell *(UICollectionView *collectionView, NSIndexPath *indexPath, id<LXNMessageProtocol> messageObject) {
        UICollectionViewCell * cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"collectionViewCell" forIndexPath:indexPath];
        // cell initialization
       	return cell;
    }];
    
[self.layoutDataSource setConfigureSupplementaryViewBlock:^UICollectionReusableView *(UICollectionView *collectionView, NSIndexPath *indexPath, NSString *kind, NSArray *sectionDataArray) {
        UICollectionReusableView * reusableView = nil;
        if ([kind isEqualToString:contactHeaderViewKind])
        {
            reusableView = [collectionView dequeueReusableSupplementaryViewOfKind:kind withReuseIdentifier:@"reuseContactHeader" forIndexPath:indexPath];
            // contact header initialization
        }
        if ([kind isEqualToString:dayHeaderViewKind])
        {
            reusableView = [collectionView dequeueReusableSupplementaryViewOfKind:kind withReuseIdentifier:@"reuseDayHeader" forIndexPath:indexPath];
            // day header initialization
        }
        return reusableView;
    }];

```
Register required headers for collection view and assing data source to it.

```objective-c
[self.collectionView registerClass:[LXNContactHeader class] forSupplementaryViewOfKind:contactHeaderViewKind withReuseIdentifier:@"reuseContactHeader"];
[self.collectionView registerClass:[LXNDayHeader class] forSupplementaryViewOfKind:dayHeaderViewKind withReuseIdentifier:@"reuseDayHeader"];
    
self.collectionView.dataSource = self.layoutDataSource;
```

You need also register cell separators as decoration view if separator height is set to greater than 0 in delegate method.
```objective-c
[self.collectionView.collectionViewLayout registerClass:[LXNMessageSeparator class] forDecorationViewOfKind:messageSeparatorViewKind];
```

Assign message array to data source. Message objects should implement LXNMessageProtocol.

```objective-c
self.layoutDataSource.dataArray = messages;
[self.collectionView reloadData];
```

LXNChatDataSourceDelegate requires implementing following methods
```objective-c
@required
- (CGSize)chatDataSource:(LXNChatDataSource *)chatDataSource cellSizeForMessage:(id<LXNMessageProtocol>)message;
- (CGSize)chatDataSource:(LXNChatDataSource *)chatDataSource dayHeaderSizeForSectionDataArray:(NSArray *)sectionArray previousSectionDataArray:(NSArray *)prevSectionArray;
- (CGSize)chatDataSource:(LXNChatDataSource *)chatDataSource contactHeaderSizeForSectionDataArray:(NSArray *)array;
- (BOOL)chatDataSource:(LXNChatDataSource *)chatDataSource contactHeaderOnLeftForSectionDataArray:(NSArray *)array;
@optional
- (CGFloat)chatDataSource:(LXNChatDataSource *)chatDataSource cellOffsetFromContactHeaderForMessage:(id<LXNMessageProtocol>)message;
- (UIEdgeInsets)cellSeparatorInsetsChatDataSource:(LXNChatDataSource *)chatDataSource;
- (CGFloat)cellSeparatorHeightForChatDataSource:(LXNChatDataSource *)chatDataSource;
- (BOOL)cellSeparatorIsPartOfCellForChatDataSource:(LXNChatDataSource *)chatDataSource;
```

## Updating

### Notes