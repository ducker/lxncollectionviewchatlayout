//
//  LXNMessageTest.m
//  Chat
//
//  Created by Leszek Kaczor on 08/07/14.
//  Copyright (c) 2014 Untitled Kingdom. All rights reserved.
//

#import "LXNMessageTest.h"

@implementation LXNMessageTest

+ (instancetype)getDefault
{
    LXNMessageTest * obj = [[LXNMessageTest alloc] init];
    obj.message = @"test message";
    obj.authorId = @"ABC";
    obj.authorName = @"ABC CDE";
    obj.authorImageURL = nil;
    obj.date = [NSDate date];
    return obj;
}

+ (instancetype)getDefaultWithChangedAuthor
{
    LXNMessageTest * obj = [[LXNMessageTest alloc] init];
    obj.message = @"test message";
    obj.authorId = @"CDE";
    obj.authorName = @"CDE FGH";
    obj.authorImageURL = nil;
    obj.date = [NSDate date];
    return obj;
}

+ (instancetype)getDefaultWithChangedDate
{
    LXNMessageTest * obj = [[LXNMessageTest alloc] init];
    obj.message = @"test message";
    obj.authorId = @"ABC";
    obj.authorName = @"ABC CDE";
    obj.authorImageURL = nil;
    obj.date = [[NSDate date] dateByAddingTimeInterval:60*60*24];
    return obj;
}

+ (instancetype)getDefaultWithChangedDateAndAuthor
{
    LXNMessageTest * obj = [[LXNMessageTest alloc] init];
    obj.message = @"test message";
    obj.authorId = @"CDE";
    obj.authorName = @"CDE FGH";
    obj.authorImageURL = nil;
    obj.date = [[NSDate date] dateByAddingTimeInterval:60*60*24];
    return obj;
}


@end
