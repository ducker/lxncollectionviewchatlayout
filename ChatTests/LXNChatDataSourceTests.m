//
//  LXNChatDataSourceTests.m
//  Chat
//
//  Created by Leszek Kaczor on 07/07/14.
//  Copyright (c) 2014 Untitled Kingdom. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "LXNChatDataSource.h"
#import "LXNMessageTest.h"

@interface LXNChatDataSource(private)

@property (strong, nonatomic) NSArray * localDataArray;

- (void)configureLocalDataArray:(NSArray *)array;
- (NSArray *)groupMessagesByDateFromArray:(NSArray *)array;
- (NSDate *)getDateWithoutHoursFromDate:(NSDate *)date;
- (NSArray *)groupMessagesByAuthorFromArray:(NSArray *)array;

- (id<LXNMessageProtocol>)objectForIndexPath:(NSIndexPath *)indexPath;
- (NSArray *)sectionDataArrayForIndex:(NSInteger)section;
- (NSInteger)numberOfSections;

@end

@interface LXNChatDataSourceTests : XCTestCase

@property (strong, nonatomic) LXNChatDataSource * dataSource;

@end

@implementation LXNChatDataSourceTests

- (void)setUp
{
    [super setUp];
    self.dataSource = [[LXNChatDataSource alloc] init];
}

- (void)tearDown
{
    self.dataSource = nil;
    [super tearDown];
}

- (void)testGroupMessagesByDay
{
    LXNMessageTest * obj = [LXNMessageTest getDefault];
    LXNMessageTest * obj2 = [LXNMessageTest getDefault];
    LXNMessageTest * obj3 = [LXNMessageTest getDefaultWithChangedDate];
    LXNMessageTest * obj4 = [LXNMessageTest getDefaultWithChangedDate];
    
    NSArray * groupped = [self.dataSource groupMessagesByDateFromArray:@[obj, obj2, obj3, obj4]];
    XCTAssert(groupped.count == 2, @"Failed groupping by days: %ld != 2", (unsigned long)groupped.count);
    for (NSArray * arr in groupped)
        XCTAssert(arr.count == 2, @"Missing messages: %ld != 2", (unsigned long)arr.count);
}

- (void)testGroupMessagesByAuthor
{
    LXNMessageTest * obj = [LXNMessageTest getDefault];
    LXNMessageTest * obj2 = [LXNMessageTest getDefault];
    LXNMessageTest * obj3 = [LXNMessageTest getDefaultWithChangedAuthor];
    LXNMessageTest * obj4 = [LXNMessageTest getDefaultWithChangedAuthor];
    
    NSArray * groupped = [self.dataSource groupMessagesByAuthorFromArray:@[obj, obj2, obj3, obj4]];
    XCTAssert(groupped.count == 2, @"Failed groupping by authors: %ld != 2", (unsigned long)groupped.count);
    for (NSArray * arr in groupped)
        XCTAssert(arr.count == 2, @"Missing messages: %ld != 2", (unsigned long)arr.count);
}

- (void)testGroupMessagesByAuthorWithMultipleConversations
{
    LXNMessageTest * obj = [LXNMessageTest getDefault];
    LXNMessageTest * obj2 = [LXNMessageTest getDefault];
    LXNMessageTest * obj3 = [LXNMessageTest getDefaultWithChangedAuthor];
    LXNMessageTest * obj4 = [LXNMessageTest getDefaultWithChangedAuthor];
    
    LXNMessageTest * obj5 = [LXNMessageTest getDefault];
    LXNMessageTest * obj6 = [LXNMessageTest getDefault];
    LXNMessageTest * obj7 = [LXNMessageTest getDefaultWithChangedAuthor];
    LXNMessageTest * obj8 = [LXNMessageTest getDefaultWithChangedAuthor];
    
    NSArray * groupped = [self.dataSource groupMessagesByAuthorFromArray:@[obj, obj2, obj3, obj4, obj5, obj6, obj7, obj8]];
    XCTAssert(groupped.count == 4, @"Failed groupping by authors: %ld != 4", (unsigned long)groupped.count);
    for (NSArray * arr in groupped)
        XCTAssert(arr.count == 2, @"Missing messages: %ld != 2", (unsigned long)arr.count);
}

- (void)testConfigureLocalArray
{
    LXNMessageTest * obj = [LXNMessageTest getDefault];
    LXNMessageTest * obj2 = [LXNMessageTest getDefault];
    LXNMessageTest * obj3 = [LXNMessageTest getDefaultWithChangedAuthor];
    LXNMessageTest * obj4 = [LXNMessageTest getDefaultWithChangedAuthor];
    
    LXNMessageTest * obj5 = [LXNMessageTest getDefaultWithChangedDate];
    LXNMessageTest * obj6 = [LXNMessageTest getDefaultWithChangedDate];
    LXNMessageTest * obj7 = [LXNMessageTest getDefaultWithChangedDateAndAuthor];
    LXNMessageTest * obj8 = [LXNMessageTest getDefaultWithChangedDateAndAuthor];
    
    NSArray * testArray = @[obj, obj2, obj3, obj4, obj5, obj6, obj7, obj8];
    self.dataSource.dataArray = testArray;
    XCTAssert(self.dataSource.localDataArray.count == 2, @"Failed groupping by days: %ld != 2", (unsigned long)self.dataSource.localDataArray.count);
    
    for (NSArray * array in self.dataSource.localDataArray)
    {
        XCTAssert(array.count == 2, @"Failed groupping by authors: %ld != 2", (unsigned long)array.count);
        for (NSArray * arr in array)
            XCTAssert(arr.count == 2, @"Missing messages: %ld != 2", (unsigned long)arr.count);
    }
}

- (void)testCountSections
{
    LXNMessageTest * obj = [LXNMessageTest getDefault];
    LXNMessageTest * obj2 = [LXNMessageTest getDefault];
    LXNMessageTest * obj3 = [LXNMessageTest getDefaultWithChangedAuthor];
    LXNMessageTest * obj4 = [LXNMessageTest getDefaultWithChangedAuthor];
    
    LXNMessageTest * obj5 = [LXNMessageTest getDefaultWithChangedDate];
    LXNMessageTest * obj6 = [LXNMessageTest getDefaultWithChangedDate];
    LXNMessageTest * obj7 = [LXNMessageTest getDefaultWithChangedDateAndAuthor];
    LXNMessageTest * obj8 = [LXNMessageTest getDefaultWithChangedDateAndAuthor];
    
    NSArray * testArray = @[obj, obj2, obj3, obj4, obj5, obj6, obj7, obj8];
    self.dataSource.dataArray = testArray;
    XCTAssert([self.dataSource numberOfSections] == 4, @"Counting sections failed: %ld != 4", (unsigned long)self.dataSource.localDataArray.count);
}

- (void)testFetchingDataArrayForSection
{
    LXNMessageTest * obj = [LXNMessageTest getDefault];
    LXNMessageTest * obj2 = [LXNMessageTest getDefault];
    LXNMessageTest * obj3 = [LXNMessageTest getDefaultWithChangedAuthor];
    LXNMessageTest * obj4 = [LXNMessageTest getDefaultWithChangedAuthor];
    
    LXNMessageTest * obj5 = [LXNMessageTest getDefaultWithChangedDate];
    LXNMessageTest * obj6 = [LXNMessageTest getDefaultWithChangedDate];
    LXNMessageTest * obj7 = [LXNMessageTest getDefaultWithChangedDateAndAuthor];
    LXNMessageTest * obj8 = [LXNMessageTest getDefaultWithChangedDateAndAuthor];
    
    NSArray * testArray = @[obj, obj2, obj3, obj4, obj5, obj6, obj7, obj8];
    self.dataSource.dataArray = testArray;
    NSArray * sectionOne = @[obj, obj2];
    XCTAssert([[self.dataSource sectionDataArrayForIndex:0] isEqualToArray:sectionOne], @"Wrong objects in section data soure: \n%@ \nShould be: \n%@", [self.dataSource sectionDataArrayForIndex:0], sectionOne);
    NSArray * sectionTwo = @[obj3, obj4];
    XCTAssert([[self.dataSource sectionDataArrayForIndex:1] isEqualToArray:sectionTwo], @"Wrong objects in section data soure: \n%@ \nShould be: \n%@", [self.dataSource sectionDataArrayForIndex:1], sectionTwo);
    NSArray * sectionThree = @[obj5, obj6];
    XCTAssert([[self.dataSource sectionDataArrayForIndex:2] isEqualToArray:sectionThree], @"Wrong objects in section data soure: \n%@ \nShould be: \n%@", [self.dataSource sectionDataArrayForIndex:2], sectionThree);
    NSArray * sectionFour = @[obj7, obj8];
    XCTAssert([[self.dataSource sectionDataArrayForIndex:3] isEqualToArray:sectionFour], @"Wrong objects in section data soure: \n%@ \nShould be: \n%@", [self.dataSource sectionDataArrayForIndex:3], sectionFour);
}

- (void)testFetchingMessageObjects
{
    LXNMessageTest * obj = [LXNMessageTest getDefault];
    LXNMessageTest * obj2 = [LXNMessageTest getDefault];
    LXNMessageTest * obj3 = [LXNMessageTest getDefaultWithChangedAuthor];
    LXNMessageTest * obj4 = [LXNMessageTest getDefaultWithChangedAuthor];
    
    LXNMessageTest * obj5 = [LXNMessageTest getDefaultWithChangedDate];
    LXNMessageTest * obj6 = [LXNMessageTest getDefaultWithChangedDate];
    LXNMessageTest * obj7 = [LXNMessageTest getDefaultWithChangedDateAndAuthor];
    LXNMessageTest * obj8 = [LXNMessageTest getDefaultWithChangedDateAndAuthor];
    
    NSArray * testArray = @[obj, obj2, obj3, obj4, obj5, obj6, obj7, obj8];
    self.dataSource.dataArray = testArray;
    id object = [self.dataSource objectForIndexPath:[NSIndexPath indexPathForItem:0 inSection:0]];
    XCTAssert( object == obj, @"Wrong object: \n%@ \nShould be: \n%@", object, obj);
    
    self.dataSource.dataArray = testArray;
    id object2 = [self.dataSource objectForIndexPath:[NSIndexPath indexPathForItem:1 inSection:0]];
    XCTAssert( object2 == obj2, @"Wrong object: \n%@ \nShould be: \n%@", object2, obj2);
    
    self.dataSource.dataArray = testArray;
    id object3 = [self.dataSource objectForIndexPath:[NSIndexPath indexPathForItem:0 inSection:1]];
    XCTAssert( object3 == obj3, @"Wrong object: \n%@ \nShould be: \n%@", object3, obj3);
    
    self.dataSource.dataArray = testArray;
    id object4 = [self.dataSource objectForIndexPath:[NSIndexPath indexPathForItem:1 inSection:1]];
    XCTAssert( object4 == obj4, @"Wrong object: \n%@ \nShould be: \n%@", object4, obj4);
    
    self.dataSource.dataArray = testArray;
    id object5 = [self.dataSource objectForIndexPath:[NSIndexPath indexPathForItem:0 inSection:2]];
    XCTAssert( object5 == obj5, @"Wrong object: \n%@ \nShould be: \n%@", object5, obj5);
    
    self.dataSource.dataArray = testArray;
    id object6 = [self.dataSource objectForIndexPath:[NSIndexPath indexPathForItem:1 inSection:2]];
    XCTAssert( object6 == obj6, @"Wrong object: \n%@ \nShould be: \n%@", object6, obj6);
    
    self.dataSource.dataArray = testArray;
    id object7 = [self.dataSource objectForIndexPath:[NSIndexPath indexPathForItem:0 inSection:3]];
    XCTAssert( object7 == obj7, @"Wrong object: \n%@ \nShould be: \n%@", object7, obj7);
    
    self.dataSource.dataArray = testArray;
    id object8 = [self.dataSource objectForIndexPath:[NSIndexPath indexPathForItem:1 inSection:3]];
    XCTAssert( object8 == obj8, @"Wrong object: \n%@ \nShould be: \n%@", object8, obj8);
}

@end
